# OpenML dataset: insurance_6

https://www.openml.org/d/45148

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Insurance Bayesian Network. Sample 6.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-medium.html#insurance)

- Number of nodes: 27

- Number of arcs: 52

- Number of parameters: 1008

- Average Markov blanket size: 5.19

- Average degree: 3.85

- Maximum in-degree: 3

**Authors**: J. Binder, D. Koller, S. Russell, and K. Kanazawa

**Please cite**: ([URL](https://cse.sc.edu/~mgv/csce582sp21/links/SDLC_CHILD_1993.pdf)): J. Binder, D. Koller, S. Russell, and K. Kanazawa. Adaptive Probabilistic Networks with Hidden Variables. Machine Learning, 29(2-3):213-244, 1997.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45148) of an [OpenML dataset](https://www.openml.org/d/45148). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45148/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45148/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45148/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

